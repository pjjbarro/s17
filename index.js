// Mini-Activity

let placesToVisit = ["Santorini","Grand Canyon","Plitvice Lakes National Park","Golden Gate Bridge","Great Barrier Reef","Fisherman's Wharf","Tokyo Tower"];
console.log(placesToVisit[0]);
console.log(placesToVisit[placesToVisit.length-1]);
for(let i= 0; i < placesToVisit.length; i++){
	console.log(placesToVisit[i]);
}

// Array Manipulation

	// Re-assign the values/items in an array:
	placesToVisit[2] = "Giza Sphinx";
	console.log(placesToVisit);
	console.log(placesToVisit[2]);

	placesToVisit[5] = "Akihabara";
	console.log(placesToVisit);
	console.log(placesToVisit[5]);

	// Mini-Activity

	placesToVisit[0] = "Catanduanes";
	placesToVisit[placesToVisit.length-1] = "CNHS"
	console.log(placesToVisit);
	console.log(placesToVisit[0]);
	console.log(placesToVisit[placesToVisit.length-1]);

	// Adding items in an array without using methods:
	let array = [];
	console.log(array[0]);
	array[0] = "Cloud Strife";
	console.log(array);
	console.log(array[1]);
	array[1] = "Tifa Lockhart";
	console.log(array[1]);
	// How can I reliably add items at the end of array without using methods?
	/*array[array.length-1] = "Aerith Gainsborugh" - will overwrite the last item instead;*/
	array[array.length] = "Vincent Valentine";
	console.log(array);

	// Array methods
		// Manipulate arrays with pre-determined JS functions.

		// Mutators - these array methods usually change the original array.

		let array1 = ["James","Curry","Durant","Bryant"];
		// without method
		array1[array1.length] = "Doncic";
		// .push() - allows us to add an element at end of the array.
		array1.push("Lillard");
		console.log(array1);
		array1.push("Thompson");
		console.log(array1[array1.length-1]);
		// .pop() - allows us to delete or remove the last item at the end of the array.
		array1.pop();
		console.log(array1);
		//.pop() is also able to return the item we removed.
		console.log(array1.pop());
		console.log(array1);

		let removedItem = array1.pop();
		console.log(array1);
		console.log(removedItem);

		//shift() - remove the first item in the array.
		array1.shift();
		console.log(array1);

			//shift() returns the item we removed.
			let removedItemShift = array1.shift();
			console.log(array1);
			console.log(removedItemShift);

			// unshift() - adds one or more elements/items at the start of the array.
			array1.unshift("Irving");
			console.log(array1);
			array1.unshift("Paul","Booker");
			console.log(array1);

			// Mini-Activity

			array1.shift();
			console.log(array1);
			array1.pop();
			console.log(array1);
			array1.unshift("P.George");
			console.log(array1);
			array1.push("Michael CW");
			console.log(array1);

			// .sort() - By default, will allow us to sort our items in ascending order.
			array1.sort();
			console.log(array1);
			let numArray = [3,2,1,6,7,9];
			numArray.sort();
			console.log(numArray);
			let numArray2 = [32,400,450,2,9,50,90];
			// numArray.sort();
			// console.log(numArray2);
			// .sort() converts all items into strings and then arranges the items/elements accordingly as if they were words/text.

			// To be able to properly sort our numbers per value we have to add an additional function/anonymous function inside sort().

			// You can pass function as arguments to other functions.
			// You can also pass anonymous functions into other functions
			// Anonymous functions are functions without names they cannot be invoked or called and instead only works when used or given as a argument to another function.
			numArray2.sort(function(a,b){

				return a-b

			})

			console.log(numArray2);

			// descending sort per number's value
			numArray2.sort(function (a,b){

				return b-a

			})

			console.log(numArray2);

			let arrayStr = ["Marie","Zen","Jamie","Elaine"];
			/*arrayStr.sort(function(a,b){

				return b-a

			});
			console.log(arrayStr);*/

			// .sort() sorted items alphabetically
			// .reverse () reversed the order of the items
			arrayStr.sort().reverse();
			console.log(arrayStr)
			// result: strings ordered descending.

			// reverse() - reverses the order of items

			let beatles = ["George","John","Paul","Ringo"];
			beatles.reverse();
			console.log(beatles);

			// splice() - allows us to remove and add elements from a given index.
			let warriorsPlayers = ["Curry","Thompson","Green","Wiggins","Wiseman"]
			// syntax: array.splice(startingIndex,numberOfItemsToBeDeleted,elementsToAdd)
			warriorsPlayers.splice(0,0,"Iguodala");
			console.log(warriorsPlayers);
			warriorsPlayers.splice(0,1);
			console.log(warriorsPlayers);
			warriorsPlayers.splice(0,3);
			console.log(warriorsPlayers);
			warriorsPlayers.splice(1,1);
			console.log(warriorsPlayers); 
			warriorsPlayers.splice(1,0,"Looney","Livingston");
			console.log(warriorsPlayers);

			// Mini-activity

			let computerBrands = ["IBM","HP","Apple","MSI"];
			computerBrands.splice(2,2,"Lenovo","Acer","Ryzzen");
			console.log(computerBrands);
		
			// Non-mutator
				// methods that will not change the original array

				// slice() allows us to get a portion of the original array and return a new array with the items selected from the original.
				// Syntax: slice(startIndex,endIndex)
				let newBrands = computerBrands.slice(1,3);
				console.log(computerBrands);
				console.log(newBrands);
				// endIndex in slice, means just before that endIndex
				let fonts = ["Times New Roman","Comic Sans MS","Impact","Monotype Corsiva","Arial","Arial Black"];
				
				let newFontSet = fonts.slice(1,4);
				console.log(newFontSet);
				
				// if endIndex not provided, by default, will copy from startingIndex to end.
				let newFontSet2 = fonts.slice(2);
				console.log(newFontSet2)

				// If startIndex is not all provided will copy from index 0 to end.
				let newFontSet3 = fonts.slice();
				newFontSet3.reverse();
				console.log(newFontSet3);


				// Mini-Activity

				let videoGameConsoles = ["PS4","PS5","Wii","Switch","Xbox","Xbox One"];
				let microsoft = videoGameConsoles.slice(4);
				let nintendo = videoGameConsoles.slice(2,4);
				console.log(microsoft);
				console.log(nintendo);

				// .stoString() - convert the array into a single value as a string but each item will be separated by a comma
				// Syntax: array.toString()

				let sentence = ["I","Like","Javascript",".",",","It's","fun","."];
				let sentenceString = sentence.toString();
				console.log(sentenceString);

				// .join() - converts the array into a single value as a string but separator can be specified
				// Syntax: array.join(separator)
				let sentence2 = ["My","Favorite","Fast food", "is","Chowking"];
				let sentenceString2 = sentence2.join(" ");
				console.log(sentenceString2);
				let sentenceString3 = sentence2.join("");
				console.log(sentenceString3)


				// Mini-Activity

				let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];
				let name1 = charArr.slice(5,11).join("");
				let name2 = charArr.slice(13,19).join("");
				console.log(name1,name2);


				// .concat( - it combines 2 or more arrays without affecting the original arrays.
				// Syntax: array.concat(array1,array2);

				let tasksFriday = ["drink HTML","eat Javascript"];
				let tasksSaturday = ["inhale CSS","breathe Bootstrap"];
				let tasksSunday = ["Get Git","Be Node"];

				let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
				console.log(weekendTasks);

				// Accessors
				//Methods that allow us to access our array.
				// indexOf - finds the index of the given element/item when it is first found from the left.

				let batch123 = [

					"Prince",
					"Jb",
					"Jeric",
					"Kim",
					"louise",
					"Kim",
					"Efren",
					"JonMichael",
					"Dan",
					"Arnon",
					"Ian",
					"Jeric"	

				];

				console.log(batch123.indexOf("Arnon"));//7 - number types are returned
				console.log(batch123.indexOf("Efren"));//4

				// lastIndexOf() finds the index of the given element/item when it is last found from the right.
				console.log(batch123.lastIndexOf("Jeric"));//11
				console.log(batch123.lastIndexOf("Kim"));//5



				// Mini-activity

				let carBrands = [


					"BMW",
					"Dodge",
					"Dodge",
					"Maserati",
					"Porsche",
					"Chevrolet",
					"Ferrari",
					"GMC",
					"Porsche",
					"Mitsubishi",
					"Toyota",
					"Volkswagen",
					"BMW",
				];

				function indexFinder(brand){
					console.log(carBrands.indexOf(brand));
				};

				function lastIndexFinder(brand){
					console.log(carBrands.lastIndexOf(brand));
				};


				// Iterator Methods
					// These methods iterate over the items in an array much like a loop. 
					// However, with our iterator methods there also that allows to not only iterate over items but also additional instructions.




					let avengers = [

						"Hulk",
						"Black Widow",
						"Hawkeye",
						"Spider-man",
						"Iron man",
						"Captain America"


					];

					// forEach() is similar to for loop but is used on arrays. It will allow us to iterate over each item in an array and even add instruction per iteration.
					// Anonymous function within forEach will we able to recieve each and every item in an array.
					// forEach() will be able to iterate over all the items in an array without even having to indicate a condition. It will just simply stop if it was able to loop over all the items in the array

					avengers.forEach(function(avenger){

						console.log(avenger);
					});

					let marvelHeroes = ["Moon Knight","Jessica Jones","Deadpool","Cyclops"];

					// iterate over all the items in marvel heroes array and let them join the avengers
					marvelHeroes.forEach(function(hero){

						if(hero !== "Cyclops" && hero !== "Deadpool"){
							avengers.push(hero);
						}
						
					})

					console.log(avengers);

					// map() - similar to forEach however it returns a new array.

					let numbers = [25,50,30,10,5]

					let mappedNumbers = numbers.map(function(number){

						console.log(number);
						return number*5;


					})

					console.log(mappedNumbers);

					// every() - iterates over all the items and checks if all the elements passes a given condition.

					let allMemberAge = [25,30,15,20,26];

					let checkAllAdult = allMemberAge.every(function(age){

						console.log(age);

						return age >=18

					})

					console.log(checkAllAdult);//every will return true if all items passes or items being looped is able to return true. If atleast 1 of the items being looped return false, in fact, every(), will stop iterating there.

					// some() - iterate over all the items check if even atleast one of the items in the array passes the condition. Same as every(), it wil return a boolean.

					let examsScore = [75,80,74,71];
					let checkForPassing = examsScores.some(function(score){

						console.log(score);
						return score >= 80;

					});

					console.log(checkForPassing);

					// filter() - creates a new array that contains elements which passed a given condition.

					let numbersArr2 = [500,12,120,60,6,30];

					let divisibleBy5 = numbersArr2.filter(function(number){

						return number % 5 ===0

					})

					console.log(divisibleBy5);

					// find() - iterate over all the items in our array but only returns the item that will satisfy the given condition. In fact, once it finds the item passes, it will stop the process.

					let registeredUsernames = ["pedro101","mikeyTheKing2000","superPhoenix19","xxJonesxx"];

					let foundUser = registeredUsernames.find(function(username){
							console.log(username)
							return username === "superPhoenix19";
					})
					console.log(divisibleBy5);

					//find() - iterate over all the items in our array but only returns the item that will satisfy the given condition. In fact, once it finds the item that passes, it will stop the process.

		let registeredUsernames = ["pedro101","mikeyTheKing2000","superPhoenix19","xxJonesxx"];

		//find() is able to return the item that passed the condition which we can then save in a variable.
		let foundUser = registeredUsernames.find(function(username){
			console.log(username);
			return username === "superPhoenix19";
		})
		console.log(foundUser);

		//.includes() - returns a boolean true if it finds a matching item in the array. .includes() is case-sensitive

		let registeredEmails = [
			"johnnyPhoenix1991@gmail.com",
			"michaelKing@gmail.com",
			"pedro_himself@gmail.com",
			"notJonesSmith@aol.com"
		]

		let doesEmailExist = registeredEmails.includes("pedro_himself@gmail.com");
		console.log(doesEmailExist);

		/*
			Mini-Activity

			Create 2 functions
				First Function is able to find specified or the username input in our registeredUsernames array. Display the result in the console.

				Second Function is able find if a specified email already exists in the registeredEmails array. 
					-If there is an email found, show an alert:
					"Email Already Exists."
					-If there is no email found, show an alert:
					"Email is available, proceed to registration."

				You may use any of the three methods we recently discussed.

		*/


		// Mini-Activity

		function findUser(username){
			let foundUser = registeredUsernames.find(function(user){
				console.log(username);
				return username === user;
			})
			// if a user/item cannot be found or none of the items in the array matches or satisfies the condition for the find(), it will return undefined.
			console.log(foundUser);

			// if foundUser is not undefined we will show an alert
			if(foundUser){
				alert("User found!")
			}else{
				alert("User not found.")
			}
		}

		function emailExist(email){

			let doesEmailExist = registeredEmails.includes(email);

			if (doesEmailExist){
				alert("Email Already Exists.")
			} else {
				alert("Email is available, proceed to registration.")
			}
		}